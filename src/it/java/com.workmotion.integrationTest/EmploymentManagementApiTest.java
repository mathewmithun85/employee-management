package com.workmotion.integrationTest;

import com.workmotion.employeemanagement.EmployeeManagementApplication;
import com.workmotion.employeemanagement.controller.ControllerExceptionHandler;
import com.workmotion.employeemanagement.controller.EmployeeManagementController;
import com.workmotion.employeemanagement.enums.EmployeeStatus;
import com.workmotion.employeemanagement.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = EmployeeManagementApplication.class)
public class EmploymentManagementApiTest {

    private MockMvc mvc;

    @Autowired
    EmployeeManagementController employeeManagementController;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(employeeManagementController)
                .setControllerAdvice(new ControllerExceptionHandler())
                .build();
        employeeRepository.deleteAll();

    }

    @Test
    public void addEmployeeTest() throws Exception {
        String data = FileUtils.getResourceFileAsString("json/testfile.json");
        ResultActions resultActions = mvc.perform(post("/employee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(data))
                .andExpect(status().isCreated());
        assertEquals(1, employeeRepository.findAll().size());

    }

    @Test
    public void getEmployeeTest() throws Exception {
        String data = FileUtils.getResourceFileAsString("json/testfile.json");
        ResultActions resultActions = mvc.perform(post("/employee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(data))
                .andExpect(status().isCreated());
        Long l1 = employeeRepository.findAll().get(0).getId();
        //assertEquals(1l, employeeRepository.findAll().get(0).getId());
        mvc.perform(get("/employee/3"))
                .andExpect(status().isOk());
        assertEquals(1, employeeRepository.findAll().size());

    }

    @Test
    public void updateEmployeeStatusTest() throws Exception {
        String data = FileUtils.getResourceFileAsString("json/testfile.json");
        ResultActions resultActions = mvc.perform(post("/employee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(data))
                .andExpect(status().isCreated());
        assertEquals(1, employeeRepository.findAll().size());
        assertEquals(EmployeeStatus.ADDED, employeeRepository.findAll().get(0).getStatus());
        mvc.perform(put("/employee/1/APPROVED"))
                .andExpect(status().isOk());
        assertEquals(EmployeeStatus.APPROVED, employeeRepository.findAll().get(0).getStatus());
    }
}
