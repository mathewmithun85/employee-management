package com.workmotion.employeemanagement.enums;

public enum EmployeeStatus {
    ADDED,
    INCHECK,
    APPROVED,
    ACTIVE;
}
