package com.workmotion.employeemanagement.enums;

public enum ContractType {
    LIMITED,
    UNLIMITED
}
