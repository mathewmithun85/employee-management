package com.workmotion.employeemanagement.controller;

import com.workmotion.employeemanagement.dto.EmployeeDto;
import com.workmotion.employeemanagement.enums.EmployeeStatus;
import com.workmotion.employeemanagement.service.EmployeeProcess;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
public class EmployeeManagementController {
    private final EmployeeProcess employeeProcess;

    @PostMapping
    public ResponseEntity<String> addEmployee(@RequestBody EmployeeDto employeeDto) {
        return new ResponseEntity<>("New employee added with Employee Id: "
                + employeeProcess.addEmployee(employeeDto), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeDto> getEmployee(@PathVariable Long id) {
        return new ResponseEntity<>(employeeProcess.getEmployee(id), HttpStatus.OK);
    }

    @PutMapping("/{id}/{status}")
    public ResponseEntity updateEmployeeStatus(@PathVariable Long id, @PathVariable EmployeeStatus status) {
        employeeProcess.updateEmployeeStatus(id, status);
        log.info("Employee id : {} updated with the status: {}", id, status);
        return ResponseEntity.ok().build();

    }

    @DeleteMapping("/{id}")
    public void deleteEmployee(@PathVariable Long id) {
        employeeProcess.deleteEmployee(id);
        log.info("Employee id: {} deleted", id);
    }
}
