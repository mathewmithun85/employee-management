package com.workmotion.employeemanagement.service;

import com.workmotion.employeemanagement.dto.EmployeeDto;
import com.workmotion.employeemanagement.enums.EmployeeStatus;
import com.workmotion.employeemanagement.exceptions.EmployeeNotFoundException;
import com.workmotion.employeemanagement.mapper.EmployeeDataMapper;
import com.workmotion.employeemanagement.repository.EmployeeRepository;
import com.workmotion.employeemanagement.repository.model.Employee;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeProcessImpl implements EmployeeProcess {

    private final EmployeeRepository employeeRepository;

    private final EmployeeDataMapper employeeDataMapper;

    @Transactional
    @Override
    public Long addEmployee(EmployeeDto employeeDto) {
        Employee employeeToSave = employeeDataMapper.toEntity(employeeDto);
        employeeToSave.setStatus(EmployeeStatus.ADDED);
        Employee employee = employeeRepository.save(employeeToSave);
        return employee.getId();
    }

    @Override
    public EmployeeDto getEmployee(Long employeeId) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with id: " + employeeId + "not exist"));
        return employeeDataMapper.toDto(employee);
    }

    @Override
    public void updateEmployeeStatus(Long employeeId, EmployeeStatus status) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with id: " + employeeId + "not exist"));
        employee.setStatus(status);
        employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee(Long employeeId) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with id: " + employeeId + "not exist"));
        employeeRepository.delete(employee);
    }
}
