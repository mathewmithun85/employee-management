package com.workmotion.employeemanagement.service;

import com.workmotion.employeemanagement.dto.EmployeeDto;
import com.workmotion.employeemanagement.enums.EmployeeStatus;

public interface EmployeeProcess {

    Long addEmployee(EmployeeDto employeeDto);

    EmployeeDto getEmployee(Long employeeId);

    void updateEmployeeStatus(Long employeeId, EmployeeStatus status);

    void deleteEmployee(Long employeeId);

}
