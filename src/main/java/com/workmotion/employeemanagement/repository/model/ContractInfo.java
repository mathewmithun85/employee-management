package com.workmotion.employeemanagement.repository.model;

import com.workmotion.employeemanagement.enums.ContractType;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "contract_info")
public class ContractInfo extends BaseEntity {

    @Column
    Date startDate;

    @Column
    Date endDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "contract_type")
    ContractType contractType;

    @OneToOne(mappedBy = "contractInfo")
    private Employee employee;
}
