package com.workmotion.employeemanagement.repository.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.workmotion.employeemanagement.enums.EmployeeStatus;
import com.workmotion.employeemanagement.repository.model.BaseEntity;
import com.workmotion.employeemanagement.repository.model.ContractInfo;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "employee_data")
public class Employee extends BaseEntity {

    @Column
    String firstName;

    @Column
    String lastName;

    @Column
    String email;

    @Column
    String phone;

    @Column
    Date dateOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(name = "employee_status")
    EmployeeStatus status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contractInfo_id", referencedColumnName = "id")
    ContractInfo contractInfo;


}
