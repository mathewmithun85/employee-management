package com.workmotion.employeemanagement.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.workmotion.employeemanagement.enums.ContractType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContractInfoDto {

    private LocalDate startDate;
    private LocalDate endDate;
    private ContractType contractType;
}
