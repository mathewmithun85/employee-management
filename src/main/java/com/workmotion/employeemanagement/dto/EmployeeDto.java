package com.workmotion.employeemanagement.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.workmotion.employeemanagement.enums.EmployeeStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private LocalDate dateOfBirth;
    private EmployeeStatus status;
    private ContractInfoDto contractInfo;


}
