package com.workmotion.employeemanagement.mapper;

import com.workmotion.employeemanagement.dto.EmployeeDto;
import com.workmotion.employeemanagement.repository.model.Employee;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmployeeDataMapper {

    Employee toEntity(EmployeeDto employeeDto);

    EmployeeDto toDto(Employee employee);


}
