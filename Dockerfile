FROM openjdk:8-jdk-alpine
MAINTAINER mithun
COPY ./target/employee-management-0.0.1-SNAPSHOT.jar employee-management-1.0.0.jar
RUN sh -c 'touch employee-management-1.0.0.jar'
ENTRYPOINT ["java","-jar","/employee-management-1.0.0.jar"]