#### Workmotion coding-challenge

##### Requirements
JDK 8  
Maven  
Docker

##### Starting App
From the project root location, run the below commands  
##### mvn clean package  
##### docker-compose up
App running in the port 19001 (http://localhost:19001/) 

if Docker is not available, start the app by run the below command  
mvn spring-boot:run
App running in the port 9001 (http://localhost:9001/) 

##### Documentation 
Swagger Api Documentation available here  
http://localhost:19001/swagger-ui.html

If app is not running from docker, API documentation available here  
http://localhost:9001/swagger-ui.html

##### Running automated tests  
mvn verify -P integration-test

